Photos posted on the Site shall not:
* Violate the laws of honor and dignity, the rights and legitimate interests of third parties, incite religious, racial or ethnic hatred, contain scenes of violence or inhumane treatment of animals, etc.;
* Wearing obscene or offensive;
* Contain advertising of drugs;
* Violate the rights of minors;
* Violate the copyright and related rights of third parties;
* Wear a pornographic nature;
* Contain commercial advertising in any form.